from tqdm import tqdm
from bs4 import BeautifulSoup
import string
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

def GetData():
    with open('08.warc', 'rb') as f:
        text = f.read().decode("ISO-8859-1")
        text = text.lower()
        html_arr = text.split('</html>')
        final = []
        for html in tqdm(html_arr):
            bs4 = BeautifulSoup(html, 'html.parser')
            body = bs4.find('body')
            if body is not None:
                final.append(body.text)
        return final

def SplitAndProcessDoc(data):
    doc_arr = []
    for doc in tqdm(data):
        not_need_char_arr = ['\r', '\n', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '©', '«', '»', '•','—']
        for ch in not_need_char_arr:
            doc = doc.replace(ch, ' ')
        for pun in string.punctuation:
            doc = doc.replace(pun, ' ')
        doc_arr.append(doc.split())
    return doc_arr

def FindUniqueWord(split_doc_arr):
    nltk.download('wordnet')
    nltk.download('omw-1.4')
    lm = WordNetLemmatizer()
    unique_word_dict = {}
    for doc_index in tqdm(range(0, len(split_doc_arr))):
        word_arr = split_doc_arr[doc_index]
        word_dict = {}
        for word_index in range(0, len(word_arr)):
            word = lm.lemmatize(word_arr[word_index])
            if word not in word_dict.keys():
                word_dict[word] = {"count": 1, "frequency": f"<{word_index}"}
            else:
                word_dict[word]["count"] += 1
                word_dict[word]["frequency"] += f", {word_index}"
        for (key, value) in word_dict.items():
            if key not in unique_word_dict.keys():
                unique_word_dict[key] = {"count":  value['count'], "doc": f"   <{doc_index + 1},{value['count']}:{value['frequency']}>;\n"}
            else:
                unique_word_dict[key]["count"] += value['count']
                unique_word_dict[key]["doc"] += f"    {doc_index + 1},{value['count']}:{value['frequency']}>;\n"
    return unique_word_dict

def PrintWord(unique_word_dict):
    file_name = 'output.txt'
    nltk.download('stopwords')
    f = open(file_name, 'w', encoding='utf-8')
    for (key, value) in tqdm(unique_word_dict.items()):
        if key not in stopwords.words('english'):
            write_text = f"{key},{value['count']}:\n{value['doc']}"[:-1] + ">\n\n"
            f.write(write_text)
    f.close()
    # 刪除最後兩個跳行
    fin  = open(file_name, 'r', encoding='utf-8')
    fin_text_arr = fin.readlines()
    fin.close()
    fout = open(file_name, 'w', encoding='utf-8')
    new_file = ''.join(fin_text_arr[:-1])
    new_file = new_file[:-1]
    fout.write(new_file)
    fout.close()
    
if __name__ == '__main__':
    print("開始讀取文字")
    doc_arr = GetData()
    print("開始切割文字")
    split_doc_arr = SplitAndProcessDoc(doc_arr)
    print("開始找出不重複的字")
    unique_word_dict = FindUniqueWord(split_doc_arr)
    print("開始寫入檔案")
    PrintWord(unique_word_dict)
    print("完成")